﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class gmanager : MonoBehaviour
{
    //audio
    public AudioSource music_manager;
    public AudioClip current_song;
    public AudioClip main_song;
    public float music_volume = 0.5f;
    public bool MyOwn_isPlaying = false;

    //
    public static gmanager instance = null;
    public int HP = 3;
    public float TotalDistance;
    public float playerVelocity;

    public int experienceValue;
    public int crystalAmount;
    public int coinAmount;

    public int topScore_L1 = 0;
    public int topScore_L2 = 150;
    public int topScore_L3 = 300;
    public int topScore_L4 = 450;
    public int topScore_L5 = 600;
    public int topScore_L6 = 750;
    public int topScore_L7 = 900;
    public int topScore_L8 = 1050;

    public int pipesCounter;

    //Checking the level
    public int actualLevel;
    public bool levelState;

    //load the level
    public LevelData currentLevel_assetBundle;
    public PipeItemGenerator[] currentLevel_Generators;
    public PipeItem[] current_ItemPrefabsList;


    public SShipData currentLevel_playerModel_asBundle;
    public GameObject currentModel;


    public Transform current_levelGate;

    //level data
    //public Dictionary<string, LevelData> _glevelDataList = new Dictionary<string, LevelData>();
    public List<string> levels = new List<string>();
    public List<LevelData> levelsD = new List<LevelData>();

    public List<string> ships = new List<string>();
    public List<SShipData> shipsD = new List<SShipData>();

    [SerializeField]
    public List<KeyValuePair<string, LevelData>> listaMixta = new List<KeyValuePair<string, LevelData>>();
    public List<KeyValuePair<string, SShipData>> listaMixta_Ships = new List<KeyValuePair<string, SShipData>>();

    public Dictionary<string, LevelData> g_levelDataList;
    public Dictionary<string, LevelData> g_diccionario { get { return g_levelDataList; } }

    public Dictionary<string, SShipData> g_playerShipDataList;
    public Dictionary<string, SShipData> g_ship_diccionario { get { return g_playerShipDataList; } }



    //data
    private float bestScore;
    public float BestScore
    {
        get
        {
            //Some other code
            PlayerPrefs.GetFloat("bestScore", 0);
            return bestScore;
        }
        set
        {
            //Some other code
            PlayerPrefs.SetFloat("bestScore", value);
            bestScore = value;
        }
    }
    public int bestCoin;
    public int BestCoin
    {
        get
        {
            //Some other code
            PlayerPrefs.GetInt("bestCoin", 0);
            return bestCoin;
        }
        set
        {
            //Some other code
            PlayerPrefs.SetInt("bestCoin", value);
            bestCoin = value;
        }
    }
    public int bestCrystal;
    public int BestCrystal
    {
        get
        {
            //Some other code
            PlayerPrefs.GetInt("bestCrystal", 0);
            return bestCrystal;
        }
        set
        {
            //Some other code
            PlayerPrefs.SetInt("bestCrystal", value);
            bestCrystal = value;
        }
    }

    //UI
    public Text BestDistance_label;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
            


        else if (instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        //this.gameObject.AddComponent<AudioSource>();
        music_manager = this.gameObject.GetComponent<AudioSource>();
        //m.Play(0);
        

        Debug.Log("Hi there");
        gmanager.instance.currentLevel_Generators = currentLevel_assetBundle.generatorsList;
        gmanager.instance.current_ItemPrefabsList = currentLevel_assetBundle.ItemPrefabsList;
        gmanager.instance.current_levelGate = currentLevel_assetBundle.levelGateObject;


        gmanager.instance.currentModel = currentLevel_playerModel_asBundle.playerModel;

        //Audio de cada nivel -- por ahora relegado ----------------------
        current_song = currentLevel_assetBundle.themesong;

        /*music_manager.clip = current_song;
        music_manager.volume = 0.5f;
        music_manager.Play(0);*/

        //music_manager.PlayOneShot(current_song, 0.5f);
        //----------------------------------------------------------------
        
        //Cambio de planes
        //voy a dejar solo un audio en el menu
        if (!MyOwn_isPlaying)
        {
            music_manager.clip = main_song;
            music_manager.volume = 0.5f;
            music_manager.loop = true;

            music_manager.Play(0);

            MyOwn_isPlaying = true;
        }
        else
        {
            Debug.Log("ya esta rodadndo");
        }
        
    }

    private void Update()
    {
        if (gmanager.instance.TotalDistance >= gmanager.instance.topScore_L1 && gmanager.instance.TotalDistance < gmanager.instance.topScore_L2)
        {
            actualLevel = 1;
            gmanager.instance.playerVelocity = playerVelocity + 0.001f;
        }
        else if (gmanager.instance.TotalDistance > gmanager.instance.topScore_L2 && gmanager.instance.TotalDistance < gmanager.instance.topScore_L3)
        {
            actualLevel = 2;
            gmanager.instance.playerVelocity = playerVelocity + 0.001f;
        }
        else if (gmanager.instance.TotalDistance > gmanager.instance.topScore_L3 && gmanager.instance.TotalDistance < gmanager.instance.topScore_L4)
        {
            actualLevel = 3;
            gmanager.instance.playerVelocity = playerVelocity + 0.001f;
        }
        else if (gmanager.instance.TotalDistance > gmanager.instance.topScore_L5 && gmanager.instance.TotalDistance < gmanager.instance.topScore_L6)
        {
            actualLevel = 4;
            gmanager.instance.playerVelocity = playerVelocity + 0.001f;
        }
        else if (gmanager.instance.TotalDistance > gmanager.instance.topScore_L7 && gmanager.instance.TotalDistance < gmanager.instance.topScore_L8)
        {
            actualLevel = 5;
            gmanager.instance.playerVelocity = playerVelocity + 0.001f;
        }
        //Debug.Log("Level: " + actualLevel);
    }


    //addHealth
    public void AddHealth()
    {
        HP++;
    }

    public void loadData()
    {

    }

    //GET DATA
    //Experience is a basic property
    public int Experience
    {
        get
        {
            //Some other code
            return experienceValue;
        }
        set
        {
            //Some other code
            experienceValue = value;
        }
    }/**/

    //Crystal ammount
    public int Crystals
    {
        get
        {
            //Some other code
            return crystalAmount;
        }
        set
        {
            //Some other code
            crystalAmount = value;
        }
    }/**/

    //Crystal ammount
    public int coins
    {
        get
        {
            //Some other code
            return coinAmount;
        }
        set
        {
            //Some other code
            coinAmount = value;
        }
    }/**/



}