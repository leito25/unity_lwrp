﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

[CreateAssetMenu(fileName = "Ship_Data", menuName = "Twrl/Ship Data")]

public class SShipData : ScriptableObject
{
    //metadata
    [Header("Ship main Data")]
    public string shipName;
    public int cost;
    public int premiumCost;//premium account
    public Sprite shipIcon;

    [Header("Model")]
    public GameObject playerModel;
}
