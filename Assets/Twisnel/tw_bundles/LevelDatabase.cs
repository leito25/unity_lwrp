﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class LevelDatabase
{
    //
    static public Dictionary<string, LevelData> levelDataList;
    static public Dictionary<string, LevelData> diccionario { get { return levelDataList; } }

    static protected bool m_Loaded = false;
    static public bool loaded { get { return m_Loaded; } }

    static public LevelData GetLevelData(string type)
    {
        
        LevelData lista;
        if (levelDataList == null || !levelDataList.TryGetValue(type, out lista))
            return null;

        return lista;
    }


    static public IEnumerator LoadDatabase()
    {
        //if not null the dictionary was already loaded
        if (levelDataList == null)
        {
            Debug.Log("elveldata null");
            levelDataList = new Dictionary<string, LevelData>();

            

            yield return Addressables.LoadAssetsAsync<LevelData>("level_Data", op =>
            {
                if (op != null)
                {
                    if (!levelDataList.ContainsKey(op.levelName))
                        levelDataList.Add(op.levelName, op);
                        Debug.Log(op.levelName);
                        //gmanager.instance._glevelDataList.Add(op.levelName, op);
                        gmanager.instance.levels.Add(op.levelName);
                        gmanager.instance.levelsD.Add(op);
                        //gmanager.instance.listaMixta.Add(new KeyValuePair<string, LevelData>( op.levelName, op));
                        //gmanager.instance.g_levelDataList.Add(op.levelName, op);
                        
                }
            });

            m_Loaded = true;
        }

    }

}
