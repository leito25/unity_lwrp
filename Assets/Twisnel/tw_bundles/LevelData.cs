﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

[CreateAssetMenu(fileName = "LevelData", menuName = "Twrl/Level Data")]
public class LevelData : ScriptableObject
{

    //metaData
    [Header("Level main Data")]
    public string levelName;
    public int cost;
    public int premiumCost;//premium account
    public Sprite levelIcon;

    [Header("Level Texture Array")]
    public Texture tex1;
    public Texture tex2;
    public Texture tex3;
    public Texture tex4;
    public Texture tex5;

    [Header("Level Texture Normals Array")]
    public Texture tex1_N;
    public Texture tex2_N;
    public Texture tex3_N;
    public Texture tex4_N;
    public Texture tex5_N;

    //[Header("pipes")]

    [Header("Generators")]
    public PipeItemGenerator[] generatorsList;

    //[Header("palcers")]
    //placers

    [Header("Obstacles")]
    public PipeItem[] ItemPrefabsList;


    [Header("Wall Object")] 
    public Transform levelGateObject;

    public AudioClip themesong;



}
