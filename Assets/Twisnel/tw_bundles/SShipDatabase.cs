﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class SShipDatabase
{
    //diccionario
    static public Dictionary<string, SShipData> shipDataList;

    static public Dictionary<string, SShipData> diccionario { get { return shipDataList; } }


    static protected bool m_Loaded = false;

    static public bool loaded { get { return m_Loaded; } }

    static public SShipData GetShipData(string type)
    {
        SShipData lista;
        if (shipDataList == null || !shipDataList.TryGetValue(type, out lista))
            return null;

        return lista;
    }

    static public IEnumerator LoadDatabase()
    {
        //if not null  the the  dict was already loaded
        if (shipDataList == null)
        {
            Debug.Log("elveldata null");
            shipDataList = new Dictionary<string, SShipData>();



            yield return Addressables.LoadAssetsAsync<SShipData>("Ship_Data", op =>
            {
                if (op != null)
                {
                    if (!shipDataList.ContainsKey(op.shipName))
                        shipDataList.Add(op.shipName, op);
                    Debug.Log(op.shipName);
                    gmanager.instance.ships.Add(op.shipName);
                    gmanager.instance.shipsD.Add(op);

                }
            });

            m_Loaded = true;
        }
    }/**/
}
