﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Net;
using System.Net.Sockets;


public class CreateBuildAssetBundles : Editor
{

    [MenuItem("Assets/CreateAssetBundle")]
    static void ExportBundle()
    {
        string AssetBundleFolder = "AssetBundles";
        string as_platform = GetPlatformName();

        // Choose the output path according to the build target.
        string outputPath = Path.Combine(AssetBundleFolder, as_platform);
        if (!Directory.Exists(outputPath))
            Directory.CreateDirectory(outputPath);

        //@TODO: use append hash... (Make sure pipeline works correctly with it.)
        
        BuildPipeline.BuildAssetBundles(outputPath, BuildAssetBundleOptions.None, EditorUserBuildSettings.activeBuildTarget);
    }

    public static string GetPlatformName()
    {
#if UNITY_EDITOR
        return m_GetPlatform(EditorUserBuildSettings.activeBuildTarget);
#else
			return m_GetPlatform(Application.platform);
#endif
    }

    static string m_GetPlatform(BuildTarget target)
    {
        switch (target)
        {
            case BuildTarget.Android:
                return "Android";
            case BuildTarget.iOS:
                return "iOS";
            case BuildTarget.WebGL:
                return "WebGL";
            case BuildTarget.StandaloneWindows:
            case BuildTarget.StandaloneWindows64:
                return "Windows";
            case BuildTarget.StandaloneOSX:
                return "OSX";
            // Add more build targets for your own.
            // If you add more targets, don't forget to add the same platforms to GetPlatformForAssetBundles(RuntimePlatform) function.
            default:
                return null;
        }
    }

    


}

/*public class CreateBuildAssetBundles : Editor
{

    [MenuItem("Assets/CreateAssetBundle")]
    static void ExportBundle()
    {
        //string bundlePath = "Asset/AssetBundle/cosito.asset";
        //Object[] selectedAssets = Selection.GetFiltered(typeof(Object), SelectionMode.Assets);

        public string AssetBundleOutputPath = "AssetBundlesFolder";
    public string AssetsPlatform = "";



    //BuildPipeline.BuildAssetBundles("Asset/AssetBundlesFolder", BuildAssetBundleOptions.None, BuildTarget.Android);
    }

/*static string GetPlatforms(BuildTarget target)
{
    switch (target)
    {
        case BuildTarget.Android:
            return "Android";
        case BuildTarget.iOS:
            return "iOS";
        case BuildTarget.WebGL:
            return "WebGL";
        case BuildTarget.StandaloneWindows:
        case BuildTarget.StandaloneWindows64:
            return "Windows";
        case BuildTarget.StandaloneOSX:
            return "OSX";
        // Add more build targets for your own.
        // If you add more targets, don't forget to add the same platforms to GetPlatformForAssetBundles(RuntimePlatform) function.
        default:
            return null;
    }
}*/

    /*public static void BuildAssetBundles()
    {
        // Choose the output path according to the build target.
        string outputPath = Path.Combine(Utility.AssetBundlesOutputPath, Utility.GetPlatformName());
        if (!Directory.Exists(outputPath))
            Directory.CreateDirectory(outputPath);

        //@TODO: use append hash... (Make sure pipeline works correctly with it.)
        BuildPipeline.BuildAssetBundles(outputPath, BuildAssetBundleOptions.None, EditorUserBuildSettings.activeBuildTarget);
    }*/





