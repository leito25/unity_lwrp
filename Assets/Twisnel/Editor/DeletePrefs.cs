﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class DeletePrefs
{
    [MenuItem("Pmind/Twrl/DeletePrefs")]
    static void DeletePrefsF()
    {
        PlayerPrefs.DeleteAll();
    }   
}
