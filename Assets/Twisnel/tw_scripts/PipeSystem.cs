﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeSystem : MonoBehaviour
{
    public Pipe pipePrefab;//pipe new instance

    //type of texture
    public Texture myTexture_L1;
    public Texture myTexture_L2;
    public Texture myTexture_L3;
    public Texture myTexture_L4;
    public Texture myTexture_L5;
    //type of texture
    public Texture myTexture_L1_N;
    public Texture myTexture_L2_N;
    public Texture myTexture_L3_N;
    public Texture myTexture_L4_N;
    public Texture myTexture_L5_N;

    public Texture theTexture;
    public Texture theTexture_N;

    public PipeItemGenerator[] currentGenerators;



    //type of generators

    //type of prebafs objects


    public int pipeCount;//number of pipes

    private Pipe[] pipes;//Pipe array


    //
    public int emptyPipeCount;


    private void Awake()
    {
        pipes = new Pipe[pipeCount];//creaciòn del arreglo de pipes

        for (int i = 0; i < pipes.Length; i++)//estructura de varios pipes
        {
            Debug.Log("--");
            Pipe pipe = pipes[i] = Instantiate<Pipe>(pipePrefab);//instancia de Pipe en pipePrefab
            //pipe.ringDistance = Random.Range(0f, 6f);//range 0 to 360 degree// 0-2pi
            pipe.transform.SetParent(transform, false);
            pipe.Generate(/*begin pipe*/i > emptyPipeCount);//-->number of pipes at begin as parameter
            if (i > 0)
            {
                pipe.AlignWith(pipes[i - 1]);//align with the last 
            }
        }

        AlignNextPipeWithOrigin();

        theTexture = gmanager.instance.currentLevel_assetBundle.tex1;
        theTexture_N = gmanager.instance.currentLevel_assetBundle.tex1_N;
    }

    //this code allow to create the first pipe
    //to start the rotation just move the origin to the xtre od the curve radio
    public Pipe SetupFirstPipe()
    {
        transform.localPosition = new Vector3(0f, -pipes[1].CurveRadius);//cambio a 1 en el array
        return pipes[1];//cambio a 1 en el array
    }

    public void checkLevel()
    {
        switch (gmanager.instance.actualLevel)
        {
            case 5:
                //theTexture = myTexture_L5;
                theTexture = gmanager.instance.currentLevel_assetBundle.tex5;
                //theTexture_N = myTexture_L5_N;
                theTexture_N = gmanager.instance.currentLevel_assetBundle.tex5_N;
                gmanager.instance.playerVelocity = gmanager.instance.playerVelocity + 0.05f;
                break;
            case 4:
                theTexture = gmanager.instance.currentLevel_assetBundle.tex4;
                theTexture_N = gmanager.instance.currentLevel_assetBundle.tex4_N;
                gmanager.instance.playerVelocity = gmanager.instance.playerVelocity + 0.05f;
                break;
            case 3:
                theTexture = gmanager.instance.currentLevel_assetBundle.tex3;
                theTexture_N = gmanager.instance.currentLevel_assetBundle.tex3_N;
                gmanager.instance.playerVelocity = gmanager.instance.playerVelocity + 0.05f;
                break;
            case 2:
                theTexture = gmanager.instance.currentLevel_assetBundle.tex2;
                theTexture_N = gmanager.instance.currentLevel_assetBundle.tex2_N;
                gmanager.instance.playerVelocity = gmanager.instance.playerVelocity + 0.05f;
                break;
            case 1:
                theTexture = gmanager.instance.currentLevel_assetBundle.tex1;
                theTexture_N = gmanager.instance.currentLevel_assetBundle.tex1_N;
                gmanager.instance.playerVelocity = gmanager.instance.playerVelocity + 0.05f;
                break;
            default:
                theTexture = gmanager.instance.currentLevel_assetBundle.tex1;
                theTexture_N = gmanager.instance.currentLevel_assetBundle.tex1_N;
                break;
        }
    }

    public int lastLevel;
    public bool changeLevelState;

    public Transform levelWall = gmanager.instance.current_levelGate;
    public Transform avatarReference;

    public float waittime;

    /*public IEnumerator moving_wall_couroutine(Transform cositoWall, int local_waittime)
    {
        while (true)
        {
            cositoWall.transform.Translate(Vector3.left * Time.deltaTime * 5);
            yield return new WaitForSeconds(local_waittime);
        }  
    }*/


    public Pipe SetupNextPipe()
    {

        //Debug.Log("<<-->> " + lastLevel + gmanager.instance.actualLevel);
        if (lastLevel != gmanager.instance.actualLevel)
        {
            
            changeLevelState = true;
            Transform cosito = Instantiate(levelWall, new Vector3(3.0f, avatarReference.transform.position.y, avatarReference.transform.position.z), Quaternion.identity);
            cosito.SetParent(pipes[pipes.Length - 1].transform);



        }
        else
        {
            changeLevelState = false;
        }
        gmanager.instance.levelState = changeLevelState;

        lastLevel = gmanager.instance.actualLevel;
        checkLevel();
        //SetPipeProperties();
        ShiftPipes();
        AlignNextPipeWithOrigin();
        pipes[pipes.Length - 1].SetPipeProperties(theTexture, theTexture_N);

        pipes[pipes.Length - 1].Generate();//--

        pipes[pipes.Length - 1].AlignWith(pipes[pipes.Length - 2]);//--
        transform.localPosition = new Vector3(0f, -pipes[1].CurveRadius);//cambio a 1 en el array
        Debug.Log("Counter: " + gmanager.instance.pipesCounter++);


        return pipes[1];//cambio a 1 en el array
    }

    private void ShiftPipes()
    {
        Pipe temp = pipes[0];
        for (int i = 1; i< pipes.Length;i++)
        {
            pipes[i - 1] = pipes[i];
        }
        pipes[pipes.Length - 1] = temp;
    }

    private void AlignNextPipeWithOrigin()
    {

        Transform transformToAlign = pipes[1].transform;
        for (int i = 0; i < pipes.Length; i++)
        {
            //-->gap new pipe
            if (i != 1)
            {
                pipes[i].transform.SetParent(transformToAlign);
            }
        }

        transformToAlign.localPosition = Vector3.zero;
        transformToAlign.localRotation = Quaternion.identity;

        for(int i = 0; i < pipes.Length; i++)
        {

            if (i != 1)
            {
                pipes[i].transform.SetParent(transform);
            }
        }
    }
}
