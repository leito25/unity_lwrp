﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class AssetDownloader : MonoBehaviour
{
    public string url;
    GameObject ObjectGO;
    public Text loadingText;
    public Transform spawnPos;//posición de aparición

    void Start()
    {
        url = "https://www.dropbox.com/s/h0b460r9ffn5ov5/addressables_content_state.bin?dl=1";
        StartCoroutine(LoadBundle("dample"));
    }

    IEnumerator LoadBundle(string ObjectGO_name)
    {

        using (UnityWebRequest uwr = UnityWebRequestAssetBundle.GetAssetBundle(url))
        {
            yield return uwr.SendWebRequest();

            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.Log(uwr.error);
            }
            else
            {
                //get the download asset
                AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(uwr);

                //LevelData LevelScriptObj = bundle.LoadAsset<LevelData>("lvl_westfall");

                //Debug.Log("--> " + bundle.GetType());
                //Debug.Log("--> " + LevelScriptObj.GetType());
            }
        }

        /*while (!Caching.ready)
        {
            yield return null;
        }

        //Begin download
        UnityWebRequest mywww = UnityWebRequestAssetBundle.GetAssetBundle(url);
        yield return mywww.Send();

        AssetBundle myBundle = DownloadHandlerAssetBundle.GetContent(mywww);*/


    }

}
