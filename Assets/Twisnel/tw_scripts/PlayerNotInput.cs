﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNotInput : MonoBehaviour
{
    public PipeSystem_Loop pipeSystem;
    public float velocity;
    private PipeLoop currentPipe;

    public float Totaldistance
    {
        get
        {
            return distanceTraveled;
        }
    }

    //--- game menu
    public void StartGame()
    {
        distanceTraveled = 0f;
        //avatarRotation = 0f;
        systemRotation = 0f;
        worldRotation = 0f;
        currentPipe = pipeSystem.SetupFirstPipe();
        SetupCurrentPipe();
        gameObject.SetActive(true);
    }

    private void Awake()
    {
        Application.targetFrameRate = 1000;//framerate for mobile

        world = pipeSystem.transform.parent;
        rotater = transform.GetChild(0);
        //gameObject.SetActive(false);
    }
    //--

    private float deltaToRotation;
    private float systemRotation;

    public float rotationVelocity;

    private Transform world;
    private Transform rotater;

    private float worldRotation;
    //private float avatarRotation;


    //start
    private void Start()
    {
        //StartGame();

        world = pipeSystem.transform.parent;

        //get child --> the avatar
        rotater = transform.GetChild(0);

        currentPipe = pipeSystem.SetupFirstPipe();//pipe inicial

        //deltaToRotation = 360f / (2f * Mathf.PI * currentPipe.CurveRadius);//rotation degrees
        SetupCurrentPipe();
    }


    //scoring the game
    private float distanceTraveled;//checking the distance until lose

    private void Update()
    {
        float delta = velocity * Time.deltaTime;//delta time
        distanceTraveled += delta;//delta por la distancia

        systemRotation += delta * deltaToRotation;


        if (systemRotation >= currentPipe.CurveAngle)//reset del angulo
        {
            delta = (systemRotation - currentPipe.CurveAngle) / deltaToRotation;
            currentPipe = pipeSystem.SetupNextPipe();
            SetupCurrentPipe();
            systemRotation = delta * deltaToRotation;
        }


        //reset the systemrotation
        pipeSystem.transform.localRotation = Quaternion.Euler(0f, 0f, systemRotation);

        UpdateAvatarRotation();
   
    }

    private void UpdateAvatarRotation()
    {
        /*float rotationInput = 0f;
        if (Application.isMobilePlatform)
        {
            if (Input.touchCount == 1)
            {
                if (Input.GetTouch(0).position.x < Screen.width * 0.5f)
                {
                    rotationInput = -1f;
                }
                else
                {
                    rotationInput = 1f;
                }
            }
        }
        else
        {
            rotationInput = Input.GetAxis("Horizontal");
        }


        avatarRotation += rotationVelocity * Time.deltaTime * rotationInput;
        if(avatarRotation < 0f)
        {
            avatarRotation += 360f;
        }
        else if(avatarRotation >= 360f)
        {
            avatarRotation -= 360f;
        }

        rotater.localRotation = Quaternion.Euler(avatarRotation, 0f, 0f);*/
    }

    private void SetupCurrentPipe()
    {
        deltaToRotation = 360f / (2f * Mathf.PI * currentPipe.CurveRadius);
        worldRotation += currentPipe.RelativeRotation;
        if (worldRotation < 0f)
        {
            worldRotation += 360f;
        }else if(worldRotation >= 360f)
        {
            worldRotation -= 360f;
        }
        world.localRotation = Quaternion.Euler(worldRotation, 0f, 0f);
    }

    public MainMenu mainMenu;

    public void Die()
    {
        //mainMenu.EndGame(distanceTraveled);
        //gameObject.SetActive(false);
        
    }
}
