﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class splash_script : MonoBehaviour
{
    float timeLimit = 5.0f; // 10 seconds.
                            // Use this for initialization

    void Update()
    {
        // translate object for 10 seconds.
        if (timeLimit > 1)
        {
            // Decrease timeLimit.
            timeLimit -= Time.deltaTime;
            // translate backward.
            //transform.Translate(Vector3.back * Time.deltaTime, Space.World);
            Debug.Log(timeLimit);


        }
        else
        {
            jumpScene();
        }

    }

    void jumpScene()
    {
        Debug.Log("-");
        SceneManager.LoadScene("Assets/Twisnel/tw_scenes/mainMenu.unity");
    }
}
