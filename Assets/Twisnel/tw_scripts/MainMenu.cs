﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement;

public class MainMenu : MonoBehaviour
{


    //public Player player;
    //public Text scoreLabel;

    public Button Start_btn;

    public Text best_value;
    public Text best_coin_value;
    public Text best_crystal_value; 


    public IEnumerator cort;
    public Image panelAdvert;

    public Text currentLevelName;
    public Image currentLevelIcon;

    public Image currentShipIcon;


    //Buttons Panel
    public Button StoreBtn;

    public void showInitialAd()
    {
        if (Advertisement.IsReady("video"))
            Advertisement.Show("video");
    }


    private void Start()
    {


        Start_btn.gameObject.SetActive(true);
        StoreBtn.gameObject.SetActive(true);

        //currentLevelName.gameObject.SetActive(true);
        currentLevelIcon.gameObject.SetActive(true);
        currentShipIcon.gameObject.SetActive(true);

        /*LevelData currentLevelObj = gmanager.instance.currentLevel_assetBundle;

        currentLevelName.text = "Init Level: " + currentLevelObj.levelName;
        currentLevelIcon.sprite = currentLevelObj.levelIcon;*/

        //currentLevelName.text = "STAGE: " + gmanager.instance.currentLevel_assetBundle.levelName;
        currentLevelIcon.sprite = gmanager.instance.currentLevel_assetBundle.levelIcon;
        currentShipIcon.sprite = gmanager.instance.currentLevel_playerModel_asBundle.shipIcon;

        
        SceneManager.LoadScene("Assets/Twisnel/tw_scenes/twistnel_loop.unity", LoadSceneMode.Additive);
        //best_value.text = gmanager.instance.TotalDistance.ToString();
        gmanager.instance.BestScore = PlayerPrefs.GetFloat("bestScore", 0);
        best_value.text = gmanager.instance.BestScore.ToString();

        //Coins and crytals
        gmanager.instance.BestCoin = PlayerPrefs.GetInt("bestCoin", 0);
        best_coin_value.text = gmanager.instance.BestCoin.ToString();

        gmanager.instance.BestCrystal = PlayerPrefs.GetInt("bestCrystal", 0);
        best_crystal_value.text = gmanager.instance.BestCrystal.ToString();

        //play the song - mientras tanto se deja la por defecto
        //gmanager.instance.music_manager.Play(0);
        if(gmanager.instance.music_manager)
        {
            if (gmanager.instance.music_manager.clip != gmanager.instance.main_song)
            {
                Debug.Log("play mainsong");
                gmanager.instance.music_manager.clip = gmanager.instance.main_song;
                gmanager.instance.music_manager.Play(0);
            }
        }
    }


    //showing the store
    public void showStore()
    {
        SceneManager.LoadScene("Assets/Twisnel/tw_scenes/StoreUI.unity", LoadSceneMode.Single);
        Start_btn.gameObject.SetActive(false);
        StoreBtn.gameObject.SetActive(false);

        //currentLevelName.gameObject.SetActive(false);
        currentLevelIcon.gameObject.SetActive(false);
        currentShipIcon.gameObject.SetActive(false);
    }
    public void showStore_Models()
    {
        SceneManager.LoadScene("Assets/Twisnel/tw_scenes/StoreUI_ships.unity", LoadSceneMode.Single);
        Start_btn.gameObject.SetActive(false);
        StoreBtn.gameObject.SetActive(false);

        //currentLevelName.gameObject.SetActive(false);
        currentLevelIcon.gameObject.SetActive(false);
        currentShipIcon.gameObject.SetActive(false);
    }


    public IEnumerator showAd(float waittime)
    {

        Debug.Log("Core");

        yield return new WaitForSeconds(waittime);
        panelAdvert.gameObject.SetActive(false);
        keepGoing();
    }

    public void keepGoing()
    {

        //player.velocity = vel_temp;
        GetComponent<Rigidbody>().detectCollisions = true;
    }

    public void StartGame()
    {
        //player.StartGame();
        SceneManager.LoadScene("Assets/Twisnel/tw_scenes/twistnel.unity" );

    }

    public void EndGame(float distanceTraveled)
    {
        //scoreLabel.text = ((int)(distanceTraveled * 10f)).ToString();
        //gameObject.SetActive(true);
    }

    private void Update()
    {
        
    }
}
