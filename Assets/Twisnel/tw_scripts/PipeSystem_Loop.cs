﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeSystem_Loop : MonoBehaviour
{
    public PipeLoop pipePrefab;//pipe new instance
    public PipeLoop pipePrefab_Base;//pipe new instance
    public PipeLoop pipePrefab_L1;//pipe new instance
    public PipeLoop pipePrefab_L2;//pipe new instance

    public int pipeCount;//number of pipes

    private PipeLoop[] pipes;//Pipe array


    //
    public int emptyPipeCount;


    private void Awake()
    {
        pipes = new PipeLoop[pipeCount];//creaciòn del arreglo de pipes

        for (int i = 0; i < pipes.Length; i++)//estructura de varios pipes
        {
            Debug.Log("--");
            PipeLoop pipe = pipes[i] = Instantiate<PipeLoop>(pipePrefab);//instancia de Pipe en pipePrefab
            //pipe.ringDistance = Random.Range(0f, 6f);//range 0 to 360 degree// 0-2pi
            pipe.transform.SetParent(transform, false);
            pipe.Generate(/*begin pipe*/i > emptyPipeCount);//-->number of pipes at begin as parameter
            if (i > 0)
            {
                pipe.AlignWith(pipes[i - 1]);//align with the last 
            }
        }

        AlignNextPipeWithOrigin();
    }

    //this code allow to create the first pipe
    //to start the rotation just move the origin to the xtre od the curve radio
    public PipeLoop SetupFirstPipe()
    {
        transform.localPosition = new Vector3(0f, -pipes[1].CurveRadius);//cambio a 1 en el array
        return pipes[1];//cambio a 1 en el array
    }

    public PipeLoop SetupNextPipe()
    {
        ShiftPipes();
        AlignNextPipeWithOrigin();
        pipes[pipes.Length - 1].Generate();//--
        pipes[pipes.Length - 1].AlignWith(pipes[pipes.Length - 2]);//--
        transform.localPosition = new Vector3(0f, -pipes[1].CurveRadius);//cambio a 1 en el array
        return pipes[1];//cambio a 1 en el array
    }

    private void ShiftPipes()
    {
        PipeLoop temp = pipes[0];
        for (int i = 1; i< pipes.Length;i++)
        {
            pipes[i - 1] = pipes[i];
        }
        pipes[pipes.Length - 1] = temp;
    }

    private void AlignNextPipeWithOrigin()
    {
        Transform transformToAlign = pipes[1].transform;
        for (int i = 0; i < pipes.Length; i++)
        {
            //-->gap new pipe
            if (i != 1)
            {
                pipes[i].transform.SetParent(transformToAlign);
            }
        }

        transformToAlign.localPosition = Vector3.zero;
        transformToAlign.localRotation = Quaternion.identity;

        for(int i = 0; i < pipes.Length; i++)
        {

            if (i != 1)
            {
                pipes[i].transform.SetParent(transform);
            }
        }
    }
}
