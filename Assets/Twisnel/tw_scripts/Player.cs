﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public PipeSystem pipeSystem;
    public float velocity;
    private Pipe currentPipe;

    public float Totaldistance
    {
        get
        {
            return distanceTraveled;
        }
        set
        {
            distanceTraveled = value;
        }
    }

    //--- game menu
    public void StartGame()
    {

        distanceTraveled = 0f;
        avatarRotation = 0f;
        systemRotation = 0f;
        worldRotation = 0f;
        currentPipe = pipeSystem.SetupFirstPipe();
        SetupCurrentPipe();
        gameObject.SetActive(true);

        Application.targetFrameRate = 1000;//framerate for mobile

        world = pipeSystem.transform.parent;
        rotater = transform.GetChild(0);
    }

    private void Awake()
    {
        //gameObject.SetActive(false);
    }
    //--

    private float deltaToRotation;
    private float systemRotation;

    public float rotationVelocity;

    private Transform world;
    private Transform rotater;

    //the player model
    public Transform shapeModel;
    public Transform playerModel;

    private float worldRotation;
    private float avatarRotation;


    //start
    private void Start()
    {
        if (gmanager.instance.music_manager)
        {
            Debug.Log(" ++ ");
            gmanager.instance.music_manager.clip = gmanager.instance.current_song;
            gmanager.instance.music_manager.Play(0);
        }

        world = pipeSystem.transform.parent;

        //get child --> the avatar
        rotater = transform.GetChild(0);
        //shapeModel = rotater.Find("Shape");
        playerModel = shapeModel.GetComponent<Transform>();

        currentPipe = pipeSystem.SetupFirstPipe();//pipe inicial

        //deltaToRotation = 360f / (2f * Mathf.PI * currentPipe.CurveRadius);//rotation degrees
        SetupCurrentPipe();
    }


    //scoring the game
    private float distanceTraveled;//checking the distance until lose

    private void Update()
    {
        float delta = velocity * Time.deltaTime;//delta time
        distanceTraveled += delta;//delta por la distancia

        systemRotation += delta * deltaToRotation;


        if (systemRotation >= currentPipe.CurveAngle)//reset del angulo
        {
            delta = (systemRotation - currentPipe.CurveAngle) / deltaToRotation;
            currentPipe = pipeSystem.SetupNextPipe();
            SetupCurrentPipe();
            systemRotation = delta * deltaToRotation;
        }


        //reset the systemrotation
        pipeSystem.transform.localRotation = Quaternion.Euler(0f, 0f, systemRotation);

        UpdateAvatarRotation();

        

    }

    private void UpdateAvatarRotation()
    {
        float rotationInput = 0f;
        if (Application.isMobilePlatform)
        {
            if (Input.touchCount == 1)
            {
                if (Input.GetTouch(0).position.x < Screen.width * 0.5f)
                {
                    rotationInput = -1f;
                }
                else
                {
                    rotationInput = 1f;
                }
            }
            else
            {
                //reset
                //playerModel.localRotation = Quaternion.Euler(0f, 0f, 0f);
            }
        }
        else
        {
            rotationInput = Input.GetAxis("Horizontal");
        }


        avatarRotation += rotationVelocity * Time.deltaTime * rotationInput;

        //
        playerModel.localRotation = Quaternion.Euler(rotationVelocity * Time.deltaTime * (rotationInput * -5), 0f, 0f);

        if (avatarRotation < 0f)
        {
            avatarRotation += 360f;
        }
        else if(avatarRotation >= 360f)
        {
            avatarRotation -= 360f;
        }

        //mdoel
        if (rotationInput == 0.0f)
        {
            playerModel.localRotation = Quaternion.Euler(0f, 0f, 0f);
        }
        //

        rotater.localRotation = Quaternion.Euler(avatarRotation, 0f, 0f);

        Debug.Log(Input.GetAxis("Horizontal"));
    }

    private void SetupCurrentPipe()
    {
        deltaToRotation = 360f / (2f * Mathf.PI * currentPipe.CurveRadius);
        worldRotation += currentPipe.RelativeRotation;
        if (worldRotation < 0f)
        {
            worldRotation += 360f;
        }else if(worldRotation >= 360f)
        {
            worldRotation -= 360f;
        }
        world.localRotation = Quaternion.Euler(worldRotation, 0f, 0f);
    }

    public MainMenu mainMenu;

    public void Die()
    {
        mainMenu.EndGame(distanceTraveled);
        gameObject.SetActive(false);
        
    }
}
