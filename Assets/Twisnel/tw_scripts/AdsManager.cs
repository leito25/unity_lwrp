﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_ADS
using UnityEngine.Advertisements;
#endif

public class AdsManager : MonoBehaviour
{
    public ShowResult rewardResult;


    public ShowResult getSRewardedResult
    {
        get
        {
            //Some other code
            //PlayerPrefs.GetFloat("bestScore", 0);
            return rewardResult;
        }
        set
        {
            //Some other code
            //PlayerPrefs.SetFloat("bestScore", value);
            rewardResult = value;
        }
    }

    public static AdsManager ads_instance = null;

    string gameId = "3260357";
    public bool testMode = true;


    private void Awake()
    {
        if (ads_instance == null)
        {
            ads_instance = this;
            DontDestroyOnLoad(ads_instance);
        }



        else if (ads_instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
#if UNITY_ADS
        Advertisement.Initialize(gameId, testMode);
#endif
    }


    //show defaultadd
    public void showDefaultAd()
    {
#if UNITY_ADS
        if(!Advertisement.IsReady())
        {
            Debug.Log("Ads not ready");
            return;
        }

        Advertisement.Show();
#endif
    }

    public void ShowRewardedAd()
    {
        const string RewardPlacementId = "rewardedVideo";
        //Debug.Log("AdTyp: " );
#if UNITY_ADS
        if (!Advertisement.IsReady(RewardPlacementId))
        {
            Debug.Log(string.Format("Ads not ready '{0}'", RewardPlacementId));
            return;
        }

        var option = new ShowOptions { resultCallback = HandleShowResult };
        Advertisement.Show(RewardPlacementId, option);
#endif
    }


#if UNITY_ADS
    private void HandleShowResult(ShowResult result)
    {
        switch(result)
        {
            case ShowResult.Finished:
                rewardResult = result;
                GameObject.FindObjectOfType<Avatar>().keepGoing();
                Debug.Log("the ad successfully showed");
                break;

            case ShowResult.Skipped:
                rewardResult = result;
                Debug.Log("Ad skipped");
                break;

            case ShowResult.Failed:
                rewardResult = result;
                Debug.LogError("ad failed");
                break;
        }
    }
#endif
}
