﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PipeItemGeneratorLoop : MonoBehaviour
{
    public abstract void GenerateItems(PipeLoop pipe);
}