﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPlacer : PipeItemGenerator
{
    public PipeItem[] itemPrefabs;
    public PipeItem[] itemPrefabs_Base;

    private void Start()
    {
        itemPrefabs = gmanager.instance.current_ItemPrefabsList;
    }

    private void Update()
    {
        /*if (gmanager.instance.TotalDistance > gmanager.instance.topScore_L1 &&
            gmanager.instance.TotalDistance < gmanager.instance.topScore_L2)
        {
            Debug.Log("change: ---L1");
            itemPrefabs = itemPrefabsL1;
        }
        else if (gmanager.instance.TotalDistance > gmanager.instance.topScore_L2 &&
            gmanager.instance.TotalDistance < gmanager.instance.topScore_L3)
        {
            Debug.Log("change: ---L2");
            itemPrefabs = itemPrefabsL2;
        }*/
    }/**/

    public override void GenerateItems(Pipe pipe)
    {
        float angleStep = pipe.CurveAngle / pipe.CurveSegmentCount;

        

        /*if (gmanager.instance.TotalDistance > gmanager.instance.topScore_L1 &&
            gmanager.instance.TotalDistance < gmanager.instance.topScore_L2)
        {
            Debug.Log("change: ---L1");
            itemPrefabs = itemPrefabsL1;
        }
        else if (gmanager.instance.TotalDistance > gmanager.instance.topScore_L2 &&
            gmanager.instance.TotalDistance < gmanager.instance.topScore_L3)
        {
            Debug.Log("change: ---L2");
            itemPrefabs = itemPrefabsL2;
        }*/



        for (int i = 0; i < pipe.CurveSegmentCount; i++)
        {
            //random instantiation
            PipeItem item = Instantiate<PipeItem>(itemPrefabs[Random.Range(0, itemPrefabs.Length)]);

            //angle of the obstacule calculation
            float pipeRotation = (Random.Range(0, pipe.pipeSegmentCount) + 0.5f) * 360f / pipe.pipeSegmentCount;

            //final placement on the pipe
            item.Position(pipe, i * angleStep, pipeRotation);
        }
    }


}