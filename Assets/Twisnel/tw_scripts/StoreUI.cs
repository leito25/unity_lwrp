﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor;
using System.IO;
using UnityEngine.AddressableAssets;
using UnityEngine.Networking;

public class StoreUI : MonoBehaviour
{
    //
    //public List<LevelData> LevelList = new List<LevelData>();
    public List<LevelData> LevelList = new List<LevelData>();
    public List<string> LevelName_List = new List<string>();
    //public List characterList


    public Button SelectStartBtn;
    public Text SelectStartBtn_txt;

    public LevelData theLevelselectedLevel;


    public AssetReference thePrefabReference;

    public GameObject panelList;
    public Button btnItem;

    public IEnumerator croutine;

    public Image loadingIcon;


    public AudioClip main_song;

    public void StartGame_DiferentLevel()
    {

        //player.StartGame();
        gmanager.instance.currentLevel_assetBundle = theLevelselectedLevel;
        SceneManager.LoadScene("Assets/Twisnel/tw_scenes/twistnel.unity");

    }


    // Start is called before the first frame update
    void Start()
    {
        //hide loading image
        loadingIcon.gameObject.SetActive(true);

        //StartCoroutine(LevelDatabase.LoadDatabase());
        //gmanager.instance._glevelDataList = LevelDatabase.GetLevelData();
        //LevelData thelevel;

        Debug.Log(" -- ");
        //loadLevelsInit

        //esta lista se debe hace con botones pegandolos a un container
        //ListarLosNiveles();

        SceneManager.LoadScene("Assets/Twisnel/tw_scenes/twistnel_loop.unity", LoadSceneMode.Additive);
        ListNiveles();
        StartCoroutine(loadLevelsInit(2));



    }

    public IEnumerator loadLevelsInit(float waittime)
    {

        Debug.Log("loadLevelsInit 1");
        //ListarLosNiveles();


        yield return new WaitForSeconds(waittime);
        Debug.Log("loadLevelsInit 2");
        //ListarLosNiveles();
        StartCoroutine(ListNivelesAsync(2));

    }

    public void DownloadFromWeb(string theUrl)
    {
        //StartCoroutine(LoadFromWeb(theUrl));
        StartCoroutine(LevelDatabase.LoadDatabase());
    }

    


    IEnumerator LoadFromWeb(string myUrl)
    {
        using (UnityWebRequest uwr = UnityWebRequestAssetBundle.GetAssetBundle(myUrl))
        {
            yield return uwr.SendWebRequest();

            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.Log(uwr.error);
            }
            else
            {
                //get the download asset
                AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(uwr);

                //LevelData LevelScriptObj = bundle.LoadAsset<LevelData>("lvl_westfall");

                //Debug.Log("--> " + bundle.GetType());
                //Debug.Log("--> " + LevelScriptObj.GetType());
            }
        }
    }
    public IEnumerator ListNivelesAsync(float waittime)
    {
        loadingIcon.gameObject.SetActive(true);

        Debug.Log("--> init");
        string webURL = "https://www.dropbox.com/sh/n7vy2bz7fvaieun/AACIb-CxTiwUdCZMZYLOgO32a?dl=1";
        DownloadFromWeb(webURL);


        SelectStartBtn_txt.text = "";

        float pos_counter = 0;

        foreach (Transform child in panelList.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        if (gmanager.instance.levels.Count == 0)
        {
            //Presionar el botón de descargar
            Debug.Log(">>>Empty");
            //ListNiveles();
        }


        foreach (var item in gmanager.instance.levelsD)
        {
            pos_counter = pos_counter + 100;
            //SelectStartBtn_txt.text = SelectStartBtn_txt.text + "--" + item.ToString() + "\n";
            Debug.Log(" ** ");
            Button newItem_btn = Instantiate(btnItem, transform.position, Quaternion.identity);
            Transform ubic = panelList.transform;
            newItem_btn.transform.SetParent(ubic, true);

            Vector3 btn_pos = newItem_btn.transform.position;
            btn_pos.y = btn_pos.y + pos_counter;
            newItem_btn.transform.position = btn_pos;

            newItem_btn.GetComponentInChildren<Text>().text = item.levelName + " : " + item.cost + ".pts";

            newItem_btn.onClick.AddListener(delegate () { Comprar(item.levelName); });

        }/**/

        //hide loading image
        loadingIcon.gameObject.SetActive(false);

        yield return new WaitForSeconds(2f);
    }

    public void ListNiveles()
    {
        loadingIcon.gameObject.SetActive(true);
        Debug.Log("--> init");
        string webURL = "https://www.dropbox.com/sh/n7vy2bz7fvaieun/AACIb-CxTiwUdCZMZYLOgO32a?dl=1";
        DownloadFromWeb(webURL);


        SelectStartBtn_txt.text = "";

        float pos_counter = 0;
        
        foreach (Transform child in panelList.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        if (gmanager.instance.levels.Count == 0)
        {
            //Presionar el botón de descargar
            Debug.Log(">>>Empty");
            //ListNiveles();
        }


        foreach (var item in gmanager.instance.levelsD)
        {
            pos_counter = pos_counter + 100;
            //SelectStartBtn_txt.text = SelectStartBtn_txt.text + "--" + item.ToString() + "\n";
            Debug.Log(" ** ");
            Button newItem_btn = Instantiate(btnItem, transform.position, Quaternion.identity);
            Transform ubic = panelList.transform;
            newItem_btn.transform.SetParent(ubic, true);

            Vector3 btn_pos = newItem_btn.transform.position;
            btn_pos.y = btn_pos.y + pos_counter;
            newItem_btn.transform.position = btn_pos;

            newItem_btn.GetComponentInChildren<Text>().text = item.levelName + " : " + item.cost + ".pts";//item.levelName.ToString() + " - " + item.premiumCost.ToString(); ;

            newItem_btn.onClick.AddListener(delegate () { Comprar(item.levelName); });

        }/**/
        loadingIcon.gameObject.SetActive(false);
    }

    public void Comprar(string item_name)
    {
        Debug.Log(">> Item seleccionado: " + item_name);

        foreach (var item in gmanager.instance.levelsD)
        {
            if(item.levelName == item_name)
            {
                gmanager.instance.currentLevel_assetBundle = item;
                //level applied
                Debug.Log("level applied");

                gmanager.instance.currentLevel_Generators = item.generatorsList;
                gmanager.instance.current_ItemPrefabsList = item.ItemPrefabsList;
                gmanager.instance.current_levelGate = item.levelGateObject;
                //gmanager.instance.music_manager.clip = item.themesong;
                gmanager.instance.current_song = item.themesong;
            }
        }

        SceneManager.LoadScene("Assets/Twisnel/tw_scenes/mainMenu.unity", LoadSceneMode.Single);
        SceneManager.UnloadSceneAsync("Assets/Twisnel/tw_scenes/StoreUI.unity");

        /*SceneManager.LoadScene("Assets/Twisnel/tw_scenes/StoreUI.unity", LoadSceneMode.Additive);
        Start_btn.gameObject.SetActive(false);
        StoreBtn.gameObject.SetActive(false);

        currentLevelName.gameObject.SetActive(false);
        currentLevelIcon.gameObject.SetActive(false);*/


        //SceneManager.UnloadSceneAsync("Assets/Twisnel/tw_scenes/twistnel.unity");

        //LevelData itemSeleccionado = gmanager.instance.levelsD.Contains()
        //gmanager.instance.currentLevel_assetBundle = 
    }

    public void ListarLosNiveles()
    {
        



        Debug.Log("Listar niveles");
        //listar los niveles
        foreach (KeyValuePair<string, LevelData> pair in LevelDatabase.diccionario)
        {
            LevelData thelevel = pair.Value;
            Debug.Log("From Store: " + thelevel.levelName);

            if (thelevel != null)
            {
                thePrefabReference.InstantiateAsync().Completed += (op) =>
                {
                    if (op.Result == null || !(op.Result is GameObject))
                    {
                        Debug.LogWarning(string.Format("unable to load shoplist {0}.", thePrefabReference.Asset.name));
                        return;
                    }

                    GameObject nuevaEntry = op.Result;

                    //Debug.Log("-->" + thelevel.levelName);
                    //Debug.Log("---> " + nuevaEntry.name);
                    //LevelList.Add(thelevel);

                    //LevelName_List.Add(thelevel.levelName);

                    

                };
            }
        }
    }

    void cargarNivel(LevelData _leveldata)
    {
        //cargar el nivel
        if (gmanager.instance.levels.Contains(_leveldata.levelName))
        {
            Debug.Log("load the level");
            gmanager.instance.currentLevel_assetBundle = _leveldata;
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
