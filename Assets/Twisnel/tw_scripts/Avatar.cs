﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.Advertisements;

public class Avatar : MonoBehaviour
{
    //references
    public ParticleSystem shape, trail, burst;
    public Player player;
    public Text tfield_distance;
    public Image PanelBG;

    public Image PanelVideo_reward;
    public Image PanelVideo_impress;
    public Text PanelVideo_counter;

    float vel_temp;
    float totalDistance_temp;

    public Text velocityLabel;

    private int m_Totaldistance;

    public Text anunciosText;


    //Best score
    public Text panel_bestscore;


    public int Totaldistance
    {
        get { return m_Totaldistance; }
        set { m_Totaldistance = value; }
    }

    public void checkDistance()
    {


        tfield_distance.text = player.Totaldistance.ToString();
        gmanager.instance.TotalDistance = player.Totaldistance;
        player.velocity = player.velocity + 0.0005f;///0.01f + 0.001f;
    }

    private void Awake()
    {
        player = transform.root.GetComponent<Player>();
    }

    private void OnTriggerEnter(Collider col)
    {
        if(col.tag == "CrystalObj")
        {

            //TODO aumentar las monedas - powercubes
            gmanager.instance.coinAmount++;
            Debug.Log(gmanager.instance.coinAmount);
            Destroy(col.gameObject);

        }
        else if (col.tag == "LevelWall")
        {

            //TODO aumentar los cristales
            gmanager.instance.crystalAmount++;
            Debug.Log(gmanager.instance.crystalAmount);

            // Condition1 is false and Condition2 is true.
            //anunciosText.text = "FASTERR!!";
            StartCoroutine(fadeOut_Faster());
        }
        else { 

        saveScore();
        panel_bestscore.text = gmanager.instance.TotalDistance.ToString();

        getValueVelocity();

        pauseGame();

        Debug.Log(col.gameObject.name);
        Destroy(col.gameObject);

        GetComponent<Rigidbody>().detectCollisions = false;

        }
    }

    private IEnumerator fadeOut_Faster()
    {
        //
        anunciosText.text = "FASTERR!!";
        yield return new WaitForSeconds(2);
        anunciosText.text = "";
    }

    public void saveScore()
    {
        gmanager.instance.TotalDistance = player.Totaldistance;
        if (gmanager.instance.TotalDistance > gmanager.instance.BestScore)
        {
            gmanager.instance.BestScore = gmanager.instance.TotalDistance;
        }



        if (gmanager.instance.coinAmount > gmanager.instance.BestCoin)
        {
            gmanager.instance.BestCoin = gmanager.instance.coinAmount;
        }

        if (gmanager.instance.crystalAmount > gmanager.instance.BestCrystal)
        {
            gmanager.instance.BestCrystal = gmanager.instance.crystalAmount;
        }
    }

    private IEnumerator cor;

    public void pauseGame()
    {
        cor = showResetPanel();
        StartCoroutine(cor);
    }

    public void getValueVelocity()
    {
        gmanager.instance.playerVelocity = player.velocity;
        gmanager.instance.TotalDistance = player.Totaldistance;
        vel_temp = gmanager.instance.playerVelocity;
        totalDistance_temp = gmanager.instance.TotalDistance;
    }

    private IEnumerator showResetPanel()
    {
        player.velocity = 0.0f;
        //GetComponent<Collider>().isTrigger = false;
       //GetComponent<Rigidbody>().detectCollisions = false;
        //Debug.Log("showResetPanel - before" + vel_temp);

        PanelBG.gameObject.SetActive(true);
        yield return new WaitForSeconds(4);
        showNonRewardVideoAd();
        PlayerPrefs.SetFloat("totalDistance", gmanager.instance.TotalDistance);

        /*if (gmanager.instance.music_manager)
        {
            Debug.Log(" +nit+ ");
            gmanager.instance.music_manager.clip = gmanager.instance.main_song;
            gmanager.instance.music_manager.Play(0);
        }*/

        SceneManager.LoadScene("Assets/Twisnel/tw_scenes/mainMenu.unity");
        //SceneManager.UnloadSceneAsync("Assets/Twisnel/tw_scenes/twistnel.unity");

    }

    public void showVideo()//--------------------------------------------------------videoreward
    {
        StopAllCoroutines();
        PanelBG.gameObject.SetActive(false);
        //showRewardVideoAd();
        AdsManager.ads_instance.ShowRewardedAd();
        Debug.Log("--> runned ads");
        //cor = VideoRewardDelay(1);
        
        /*PanelVideo_reward.gameObject.SetActive(true);
        cor = showVideo(1);
        StartCoroutine(showVideo(1));*/
    }
    public IEnumerator VideoRewardDelay(float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
        ShowResult AdResult = AdsManager.ads_instance.getSRewardedResult;
        switch (AdResult)
        {
            case ShowResult.Finished:
                Debug.Log("Finished");
                keepGoing();
                break;

            case ShowResult.Skipped:
                Debug.Log("Skipped");
                goBacktoMenu();
                break;

            case ShowResult.Failed:
                Debug.Log("Failed");
                goBacktoMenu();
                break;
        }
    }

    public void restartLevel()
    {
        //
        showVideoLite("retry");
        //resettunnelLevel();
    }

    public void NewRestartLevel()
    {
        StopAllCoroutines();
        AdsManager.ads_instance.showDefaultAd();
        cor = CDelay(3);
        StartCoroutine(CDelay(3));
    }

    public IEnumerator CDelay(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        resettunnelLevel();

    }

    //resetLevel

    public void goBacktoMenu()
    {
        //
        showVideoLite("x");
        //resetLevel();
    }

    public void showVideoLite(string type)//video without reward
    {
        StopAllCoroutines();
        PanelBG.gameObject.SetActive(false);
        showNonRewardVideoAd();//videoNoreward
        //PanelVideo_impress.gameObject.SetActive(true);
        cor = showVideoLite(4, type);
        StartCoroutine(showVideoLite(4, type));
    }

    public IEnumerator showVideo(float waittime)
    {

        Debug.Log("Core_Reward");

        yield return new WaitForSeconds(waittime);
        PanelVideo_reward.gameObject.SetActive(false);
        keepGoing();//video--> resume
        //resettunnelLevel();
    }

    public void videoLite_CloseBtn(string type)
    {
        if (type == "retry")
        {
            resettunnelLevel();
        }
        else
        {
            resetLevel();
        }
    }

    public IEnumerator showVideoLite(float waittime, string type)
    {

        Debug.Log("Core_Lite");

        yield return new WaitForSeconds(waittime);
        //PanelVideo_impress.gameObject.SetActive(false);
        Debug.Log("--!--");
        if (type == "retry")
        {
            resettunnelLevel();
        }
        else
        {
            resetLevel();
        }
        //keepGoing();//video--> resume
        //resettunnelLevel();
    }

    public void watchVideo()
    {
        //
    }

    public void keepGoing()
    {

        player.velocity = vel_temp;
        GetComponent<Rigidbody>().detectCollisions = true;
    }

    public void resettunnelLevel()
    {
        StopAllCoroutines();
        gmanager.instance.TotalDistance = player.Totaldistance;
        PlayerPrefs.SetFloat("totalDistance", gmanager.instance.TotalDistance);
        SceneManager.LoadScene("Assets/Twisnel/tw_scenes/twistnel.unity");
    }

    public void resetLevel()
    {

        gmanager.instance.TotalDistance = player.Totaldistance;
        PlayerPrefs.SetFloat("totalDistance", gmanager.instance.TotalDistance);
        //
        showNonRewardVideoAd();


        SceneManager.LoadScene("Assets/Twisnel/tw_scenes/mainMenu.unity");
        //SceneManager.UnloadSceneAsync("Assets/Twisnel/tw_scenes/twistnel.unity");
    }

    public void showNonRewardVideoAd()
    {
        if (Advertisement.IsReady("video"))
            Advertisement.Show("video");
    }

    public void showRewardVideoAd()
    {
        if (Advertisement.IsReady("rewardedVideo"))
            Advertisement.Show("rewardedVideo");
    }

    private void Update()
    {
        checkDistance();
    }

    public void loading_newScene()
    {

    }
}
