﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeLoop : MonoBehaviour
{
    public float pipeRadius;
    public int pipeSegmentCount;

    public float minCurveRadius, maxCurveRadius;
    public int minCurveSegmentCount, maxCurveSegmentCount;

    private float curveRadius;
    private int curveSegmentCount;

    public float CurveRadius
    {
        get
        {
            return curveRadius;
        }
    }

    public float CurveAngle
    {
        get
        {
            return curveAngle;
        }
    }

    public int CurveSegmentCount
    {
        get
        {
            return curveSegmentCount;
        }
    }

    private float relativeRotation;

    public float RelativeRotation
    {
        get
        {
            return relativeRotation;
        }
    }

    //based in the torus geometric formula 
    //--
    //x = (R + r cos v) cos u
    //y = (R + r cos v) sin u
    //z = r cos v
    //--
    //will be created the torus
    /*private void Start()
    {
        //gameObject.AddComponent<InstanciateColor>().color = Color.cyan;

    }*/

    private Vector3 GetPointOnTorus(float u, float v)
    {
        Vector3 point;//point
        float r = (curveRadius + pipeRadius * Mathf.Cos(v));//--> R + r . cos v
        point.x = r * Mathf.Sin(u);                         //x = (R + r cos v) cos u
        point.y = r * Mathf.Cos(u);                         //y = (R + r cos v) sin u
        point.z = pipeRadius * Mathf.Sin(v);                //z = r cos v

        return point;

    }

    /*private void OnDrawGizmos()
    {
        float uStep = (2f * Mathf.PI) / curveSegmentCount;//ustep gonna be the curve segments
        float vStep = (2f * Mathf.PI) / pipeSegmentCount;//vstep gonna be the tube segments steps

        for (int u = 0; u < curveSegmentCount; u++)
        {
            for (int v = 0; v < pipeSegmentCount; v++)
            {
                Vector3 point = GetPointOnTorus(u * uStep, v * vStep); //sample 1->20 = 2, 2->20=40, 3->20=60...
                Gizmos.DrawSphere(point, 0.1f);
            }

        }

    }*/



    //---Mesh building
    private Mesh myMesh;
    private Vector3[] vertices;
    private int[] triangles;

    public Material myMat;

    public Material myMat_L1;
    public Material myMat_L2;
    public Material myMat_L3;
    public Material myMat_L4;
    public Material myMat_L5;

    private void Awake()
    {
        GetComponent<MeshFilter>().mesh = myMesh = new Mesh();
        myMesh.name = "MyPipe";

        //curveRadius = Random.Range(minCurveRadius, maxCurveRadius);
        //curveSegmentCount = Random.Range(minCurveSegmentCount, maxCurveSegmentCount + 1);

        //setVertices();
        //setTriangles();
        //StartCoroutine(setTrianglesC());
        //myMesh.RecalculateNormals();

        //gameObject.AddComponent<MeshRenderer>().material = new Material(Shader.Find("MyOwnPipeline/MyUnlit"));
        //gameObject.AddComponent<InstanciateColor>().color = Color.blue;/**/

        //gameObject.AddComponent<MeshRenderer>().material = new Material(Shader.Find("Standard"));
        //HDgameObject.AddComponent<MeshRenderer>().material = new Material(Shader.Find("Lightweight Render Pipeline/Lit"));

        /*gameObject.AddComponent<MeshRenderer>().material = new Material(Shader.Find("Lightweight Render Pipeline/Lit"));
        gameObject.GetComponent<Material>().color = Color.yellow;*/

        //gameObject.AddComponent<MeshRenderer>().material = new Material(Shader.Find("Toon/Basic"));
        gameObject.AddComponent<MeshRenderer>().material = myMat;
        //gameObject.GetComponent<Material>().color = Color.yellow;

    }

    public PipeItemGeneratorLoop[] generators;

    public void Generate(bool withItems = true)
    {


        /*if (gmanager.instance.TotalDistance > gmanager.instance.topScore_L1 &&
            gmanager.instance.TotalDistance < gmanager.instance.topScore_L2)
        {
            Debug.Log("pipe change L1: " + gmanager.instance.TotalDistance);
            myMat = myMat_L1;

        }
        else if (gmanager.instance.TotalDistance > gmanager.instance.topScore_L2 &&
           gmanager.instance.TotalDistance < gmanager.instance.topScore_L3)
        {
            Debug.Log("pipe change L2: " + gmanager.instance.TotalDistance);
            myMat = myMat_L2;
        }*/

        gameObject.GetComponent<MeshRenderer>().material = myMat;

        curveRadius = UnityEngine.Random.Range(minCurveRadius, maxCurveRadius);
        curveSegmentCount =
            UnityEngine.Random.Range(minCurveSegmentCount, maxCurveSegmentCount + 1);
        myMesh.Clear();
        setVertices();
        SetUV();
        setTriangles();
        SetNormals();
        myMesh.RecalculateNormals();

        //destroying obstacles
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }


        //empty pipes at the begin
        if (withItems)
        {

            //obstacles generation
            //generators[Random.Range(0, generators.Length)].GenerateItems(this);
            //this line send the pipe as parameter.. then the obstacules will be created

        }

    }

    private void SetNormals()
    {
        /*var normalss = new List<Vector3>();
        normalss.Add(vertices);
        myMesh.SetNormals(normalss);*/
    }

    private IEnumerator setTrianglesC()
    {
        //delay
        WaitForSeconds wait = new WaitForSeconds(1f);


        triangles = new int[pipeSegmentCount * curveSegmentCount * 6];

        //showing the triangles from inside
        for (int t = 0, i = 0; t < triangles.Length; t += 6, i += 4)
        {
            triangles[t] = i;//position in the array
            triangles[t + 1] = triangles[t + 4] = i + 1;
            triangles[t + 2] = triangles[t + 3] = i + 2;
            triangles[t + 5] = i + 3;

            myMesh.triangles = triangles;

            yield return wait;
        }
    }

    private Vector2[] uv;

    //set uvs
    private void SetUV()
    {
        uv = new Vector2[vertices.Length];
        for (int i = 0; i < vertices.Length; i += 4)
        {
            uv[i] = Vector2.zero;
            uv[i + 1] = Vector2.right;
            uv[i + 2] = Vector2.up;
            uv[i + 3] = Vector2.one;
        }
        myMesh.uv = uv;
    }



    public float ringDistance;
    private float curveAngle;


    private void setVertices()//calculata and store the vertex
    {
        vertices = new Vector3[pipeSegmentCount * curveSegmentCount * 4];//the four is the number of vertex per face

        //quad ring
        float uStep = ringDistance / curveSegmentCount;//--> u is the segment of the curve
        curveAngle = uStep * curveSegmentCount * (360f / (2f * Mathf.PI));
        CreateFirstQuadRing(uStep);

        int iDelta = pipeSegmentCount * 4;//4 vertex per segment
        for (int u = 2, i = iDelta; u <= curveSegmentCount; u++, i += iDelta)
        {
            CreateFirstQuadRing(u * uStep, i);
        }


        myMesh.vertices = vertices;
    }

    private void CreateFirstQuadRing(float u, int i)
    {
        float vStep = (2f * Mathf.PI) / pipeSegmentCount;
        int ringOffset = pipeSegmentCount * 4;//offset.. jumping from quad to quad every 4 vertex

        Vector3 vertex = GetPointOnTorus(u, 0f);

        for (int v = 1; v <= pipeSegmentCount; v++, i += 4)
        {
            vertices[i] = vertices[i - ringOffset + 2];
            vertices[i + 1] = vertices[i - ringOffset + 3];
            vertices[i + 2] = vertex;
            vertices[i + 3] = vertex = GetPointOnTorus(u, v * vStep);
        }
    }

    private void CreateFirstQuadRing(float u)
    {
        float vStep = (2f * Mathf.PI) / pipeSegmentCount;

        Vector3 vertexA = GetPointOnTorus(0f, 0f);
        Vector3 vertexB = GetPointOnTorus(u, 0f);
        for (int v = 1, i = 0; v <= pipeSegmentCount; v++, i += 4)
        {
            vertices[i] = vertexA;
            vertices[i + 1] = vertexA = GetPointOnTorus(0f, v * vStep);
            vertices[i + 2] = vertexB;
            vertices[i + 3] = vertexB = GetPointOnTorus(u, v * vStep);
        }
    }

    private void setTriangles_outside()
    {
        triangles = new int[pipeSegmentCount * curveSegmentCount * 6];

        //showing the triangles from inside
        for (int t = 0, i = 0; t < triangles.Length; t += 6, i += 4)
        {
            triangles[t] = i;//position in the array
            triangles[t + 1] = triangles[t + 4] = i + 1;
            triangles[t + 2] = triangles[t + 3] = i + 2;
            triangles[t + 5] = i + 3;

            myMesh.triangles = triangles;
            //myMesh.RecalculateNormals();
        }

    }

    private void setTriangles()
    {
        triangles = new int[pipeSegmentCount * curveSegmentCount * 6];

        //showing the triangles from inside
        for (int t = 0, i = 0; t < triangles.Length; t += 6, i += 4)
        {
            triangles[t] = i;//position in the array
            triangles[t + 1] = triangles[t + 4] = i + 2;//swep faces 1 per 2
            triangles[t + 2] = triangles[t + 3] = i + 1;//swep faces 2 per 1
            triangles[t + 5] = i + 3;

            myMesh.triangles = triangles;
            //myMesh.RecalculateNormals();
        }

    }

    public void AlignWith_A(PipeLoop pipe)
    {
        transform.SetParent(pipe.transform, false);
        transform.localRotation = Quaternion.Euler(0f, 0f, -pipe.curveAngle);
        transform.SetParent(pipe.transform.parent);
    }

    public void AlignWith(PipeLoop pipe)
    {
        relativeRotation = UnityEngine.Random.Range(0, curveSegmentCount) * 360f / pipeSegmentCount;

        transform.SetParent(pipe.transform, false);
        transform.localPosition = Vector3.zero;

        //the -pipe.curveAngle is the offset angle with the last
        transform.localRotation = Quaternion.Euler(0f, 0f, -pipe.curveAngle);

        transform.Translate(0f, pipe.curveRadius, 0f);
        transform.Rotate(relativeRotation, 0f, 0f);
        transform.Translate(0f, -curveRadius, 0f);
        transform.SetParent(pipe.transform.parent);

        transform.localScale = Vector3.one;
    }
}
