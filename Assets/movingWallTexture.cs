﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movingWallTexture : MonoBehaviour
{
    public Vector2 offset;
    public float variableValue;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Renderer>().material.SetTextureOffset("_MainTex", offset);
    }

    // Update is called once per frame
    void Update()
    {
        variableValue++;
        offset = new Vector2(offset.x, variableValue);
    }
}
