﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnWall : MonoBehaviour
{

    public GameObject prefab;
    public float velocityVar;

    public bool loop;

    public int number_to_print = 10;

    IEnumerator test()
    {
        StopCoroutine("test");
        int number_to_print_in_test = number_to_print;
        while (loop)
        {
            Debug.Log(number_to_print_in_test);
            yield return 0;
        }
    }

    IEnumerator wait_then_test()
    {
        yield return new WaitForSeconds(5);
        Debug.Log("created 2nd instance of test");
        number_to_print++;
        StartCoroutine("test");
    }

    public IEnumerator movingCouroutine(Transform cositoWall, int local_waittime)
    {
        
        while (true)
        {
            //Debug.Log("OnCoroutine: " + (int)Time.time);
            cositoWall.Translate(Vector3.left * Time.deltaTime * 5);

            yield return new WaitForSeconds(Time.deltaTime);
        }


        //cositoWall.transform.Translate(new Vector3(0.1f * velocityVar, cositoWall.transform.position.y, cositoWall.transform.position.z));
        //Debug.Log("boom" + velocityVar++);
        //yield return new WaitForSeconds(local_waittime);
        //Destroy(prefab);
    } 

    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine("test");
        //StartCoroutine("wait_then_test");

        Transform clone = Instantiate(prefab.transform, new Vector3(0f,0f,0f), Quaternion.identity);
        clone.SetParent(transform, false);
        StartCoroutine(movingCouroutine(clone, 3));/**/
    }

    // Update is called once per frame
    void Update()
    {
        //velocityVar++;
    }
}
